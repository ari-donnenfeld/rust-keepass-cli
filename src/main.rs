extern crate keepass;
extern crate rpassword;
    
use rpassword::read_password;
use std::io::Write;
use keepass::{Database, NodeRef};
use std::fs::File;
use clap::Parser;


// CLI argument parser
#[derive(Parser, Debug)]
#[clap(name="Rust KeePass Cli", version, about, long_about = None)]
///  Very simple and very fast KeePass cli tool for kdbx files which allows duplicate names.
struct Args {
    // List options
    #[clap(short, long, value_name="ID")]
    /// Show specific entry by its id
    show: Option<usize>,

    #[clap(short, long, takes_value=false)]
    /// List all entries
    list: bool,

    #[clap(short, long, value_parser)]
    /// Specify database
    database: String,

    #[clap(short, long, value_parser)]
    /// Specify key file
    keyfile: Option<String>,
}


fn main() {
    let args = Args::parse();

    let db_file_str:String = args.database;
    let key_file_opt:Option<String> = args.keyfile;
    let show = args.show;

    // Get password from io
    println!("Enter Password: ");
    std::io::stdout().flush().unwrap();
    let password = read_password().unwrap();

    // Check database file exists
    let db_file = std::path::Path::new(&db_file_str);
    if !db_file.exists() { eprintln!("Database file does not exist."); std::process::exit(2); }

    let key_file;
    let db;
    match key_file_opt {
        Some(key_file_str) => {
            key_file = std::path::Path::new(&key_file_str);
            if !key_file.exists() {
                eprintln!("Key file does not exist");
                std::process::exit(2);
            } else {
                // Decrypt KeePass database with key file
                db = Database::open(&mut File::open(db_file).expect("Failed to open file"), Some(&password.trim()), Some(&mut File::open(key_file).expect("Failed to open file")));
            }
        }
        // Decrypt KeePass database
        None => db = Database::open(&mut File::open(db_file).expect("Failed to open file"), Some(&password.trim()), None)
    }


    struct Entry {
        title: String,
        username: String,
        password: String
    }

    let mut entries : Vec<Entry> = vec![];

    // Check decryption success
    match db {
        Ok(file) => {
            // Iterate over all Groups and Nodes
            for node in &file.root {
                match node {
                    NodeRef::Group(_g) => {
                        // No current use for groups
                    },
                    NodeRef::Entry(e) => {

                        let title = e.get_title().unwrap();
                        let user = e.get_username().unwrap();
                        let pass = e.get_password().unwrap();

                        let entr = Entry {
                            title: String::from(title),
                            username: String::from(user),
                            password: String::from(pass)
                        };
                        entries.push(entr);
                    }
                }
            }

            match show {
                Some(key) => {
                    if key <= entries.len()-1 {
                        println!("Title: {}", entries[key].title);
                        println!("User: {}", entries[key].username);
                        println!("Pass: {}", entries[key].password);
                    } else {
                        eprintln!("Entry does not exist!");
                        std::process::exit(3);
                    }
                },
                None => {

                    for i in 0..entries.len() {
                        println!("{}: {} - {}", i, entries[i].title, entries[i].username);

                    }
                }

            }
        },
        Err(_error) => { eprintln!("Unable to decrypt"); std::process::exit(1); }
    };
}

