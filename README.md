# RKP cli

The Rust KeePass cli is a very simple and very fast KeePass cli tool for kdbx files.

Created to overcome the limitation of the keepassxc-cli in which entries require distinct names.

## Usage

```
rkp_cli [OPTIONS] --database <DATABASE>
```

```
OPTIONS:
    -d, --database <DATABASE>    Specify database
    -h, --help                   Print help information
    -k, --keyfile <KEYFILE>      Specify key file
    -l, --list                   List all entries
    -s, --show <ID>              Show specific entry by its id
    -V, --version                Print version information
```

Note: keyfile must not be in keyx format.

## Examples

```bash
./rkp_cli -l -d database.kdbx -k keyfile.key
./rkp_cli -l -d database.kdbx
```

```
Enter Password:

0: GitLab - clark@test.com
1: GitLab - jimmy@test.com
2: StackOverflow - jimmy@test.com
```

```bash
./rkp_cli -d database.kdbx -s 1
```

```
Title: GitLab
User: jimmy@test.com
Pass: password
```

Use in a script:

```bash
echo "password" | ./rkp_cli -d database.kdbx -s 1
```

## Compilation

```bash
cargo build --release
```
